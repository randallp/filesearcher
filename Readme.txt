FileSearcher v1.0.1
    Collaborative Computing Solutions
    Randall Price


Description:
------------
    FileSearcher is a .NET 2.0 C# Console application that searches for vulnerable
    versions of the OpenSSL for the Hearbleed bug.

    By default, FileSearch is configured with the following settings:

        Open_Results                TRUE | FALSE, open the search results output
                                    file in Notepad when search ends.

        Output_File                 Full path to the search results output file.

        Search_File                 Filename to search for.

        Search_Mapped_Drives        TRUE | FALSE, search all mapped drives.

        Show_Access_Denied_Paths    TRUE | FALSE, show a list of the paths
                                    that cannot be accessed by FileSearcher.

        Versions                    1.0.1,1.0.1a,1.0.1b,1.0.1c,1.0.1d,1.0.1e,1.0.1f


Syntax:
-------
    FileSearcher [/?] [/HELP] [/SEARCH_FILE=<filename.ext>]"
                 [/OPEN_RESULTS=TRUE|false]"
                 [/OUTPUT_FILE=<path to output file>]"
                 [/VERSION=#.#.#x[,#.#.#,...]"

where:

    /?, /HELP       Display this help screen.  Help is also displayed if any of
                    the command-line paramaters are invalid.  This parameter is
                    optional and if used all other parameters are ignored.

    /OPEN_RESULTS   Determine whether or not to open the search results file in");
                    Notepad.  The default is TRUE.");

    /OUTPUT_FILE    The full path to the file to write the search results to.
                    If the filename contains spaces you must enclose it with
                    quotation marks.  The output is plain text.

    /SEARCH_FILE    The name of the file to search for.  This must be a valid
                    filename and extension.  If the filename contains spaces
                    you must enclose it with quotation marks.  No wild card
                    characters are allowed.

    /VERSIONS       A comma-delimited list of versions to check.  For exammple:
                    \"1.0.1,1.0.1a,1.0.1b\")

The default values for the command-line parameters are:

    /OPEN_RESULTS   true
    /OUTPUT_FILE    SearchResults.txt
    /SEARCH_FILE    ssleay32.dll
    /VERSIONS       1.0.1,1.0.1a,1.0.1b,1.0.1c,1.0.1d,1.0.1e,1.0.1f


Examples:
---------
FileSearcher

    Search for all occcurrences of the file \"ssleay32.dll\" that have a version
    number of 1.0.1, 1.0.1a, 1.0.1b, 1.0.1c, 1.0.1d, 1.0.1e or 1.0.1f and write
    the search results to the file SearchResults.txt in the current folder.

FileSearcher /SEARCH_FILE=ssleay32.dll
             /VERSIONS=1.0.1,1.0.1a,1.0.1b
             /OUTPUT_FILE=C:\\Temp\\MySearchResults.txt
             /OPEN_RESULTS=false

    Search for all occcurrences of the file \"ssleay32.dll\" that have a version
    number of 1.0.1, 1.0.1a, or 1.0.1f and write the search results to the file
    C:\\Temp\\MySearchResults.txt, but do not open the file in Notepad.

FileSearcher /?

    Show the program syntax and help information.


To Run:
-------
    The executable file and configuration file are in the bin/Release folder in
    the project solution.  The files are:

        Executable:     filesearcher.git\FileSearcher\bin\Release\FileSearcher.exe
        Configuration:  filesearcher.git\FileSearcher\bin\Release\FileSearcher.exe.config

    Copy the executable file and configuration file to a folder on the computer
    you want to run on.  You can edit the configuration file to change the default
    values if you want.  To run the program, double-click the executable file or
    run it from a DOS command prompt.  The program output is displayed in a DOS
    console window.  After the program completes, the search results output file
    is opened in Notepad.
