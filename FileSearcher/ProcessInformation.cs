﻿using System;
using System.Diagnostics;
using System.Management;
using System.Reflection;

namespace FileSearcher
{
    // -------------------------------------------------------------------------
    //  Public Methods:
    //  ---------------
    //    GetParentProcessName()
    // -------------------------------------------------------------------------

    public static class ProcessInformation
    {
        #region .  GetParentProcessName()  .
        // ---------------------------------------------------------------------
        //  Routine:      GetParentProcessName()
        //
        //  Description:  This routine returns the name of the parent process
        //                that called this program. Typical parent process names
        //                that I have encountered are:
        //
        //                1.  devenv.exe   - run from the Visual Studio IDE.
        //                2.  explorer.exe - run from Windows Explorer.
        //                3.  cmd.exe      - run from a DOS command prompt.
        //                4.  winlogon.exe - run from Scheduled Tasks.
        //
        //  Parameters:   None
        //
        //  Returns:      String - the name of the parent process.
        // ---------------------------------------------------------------------
        public static string GetParentProcessName()
        {
            // Get the method name for use in the logging commands.
            string strMethodName = MethodBase.GetCurrentMethod().Name + "()";

            string strParentProcessName = string.Empty;

            try
            {
                int intCurrentProcessID = Process.GetCurrentProcess().Id;

                // Use a WMI query to get the parent process id for this process.
                using (var mos = new ManagementObjectSearcher(
                       "SELECT ParentProcessId FROM Win32_Process WHERE ProcessId = " + intCurrentProcessID.ToString()))
                {
                    foreach (var obj in mos.Get())
                    {
                        object oData = obj.Properties["ParentProcessId"].Value;
                        if (oData != null)
                        {
                            int intParentProcessId = Convert.ToInt32(oData);
                            strParentProcessName   = Process.GetProcessById(intParentProcessId).MainModule.ModuleName;
                        }
                    }
                }
            }
            catch
            {
                // Do not throw an error since this is only for a development/debugging feature.
                //// Handle all unexpected errors here.
                //string strMessage =
                //    String.Format("{0}|{1}|{2}|{3}",
                //                   Constants.RC_ERROR_NUM_UNEXPECTED_ERROR,
                //                   Constants.RC_ERROR_MSG_UNEXPECTED_ERROR + " " + ex.Message.Replace("|", ":"),
                //                   strMethodName,
                //                   ex.StackTrace);
                //Logger.LogError(strMessage);

                //// Pass my custom error up the chain.
                //throw new MyException(strMessage, ex.InnerException);
            }

            return strParentProcessName;

        }   // GetProcessInformation()
        #endregion


    }   // class ProcessInformation

}   // namespace FileSearcher
