﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;

namespace FileSearcher
{
    class Program
    {
        /// --------------------------------------------------------------------
        ///  Instance Variables:
        ///  -------------------
        ///    m_blnOpenResults
        ///    m_blnSwitch_ShowHelp
        ///    m_blnSearchMappedDrives
        ///    m_blnShowAccessDeniedPaths
        ///    m_intFileCount
        ///    m_intVulnerableCount
        ///    m_lstFilesFound
        ///    m_strFileToSearchFor
        ///    m_strOutputFile
        ///    m_strVersions
        ///    m_oStreamWriter
        ///    m_colNoAccessLog
        /// --------------------------------------------------------------------

        #region .  Instance variables  .

        static bool         m_blnOpenResults           = false;
        static bool         m_blnSearchMappedDrives    = false;
        static bool         m_blnShowAccessDeniedPaths = false;
        static bool         m_blnSwitch_ShowHelp       = false;

        static int          m_intFileCount             = 0;
        static int          m_intVulnerableCount       = 0;

        static List<string> m_lstFilesFound            = new List<string>();
                                                       
        static string       m_strFileToSearchFor       = String.Empty;
        static string       m_strOutputFile            = String.Empty;
        static string       m_strVersions              = String.Empty;

        static StreamWriter m_oStreamWriter;

        static System.Collections.Specialized.StringCollection m_colNoAccessLog = new System.Collections.Specialized.StringCollection();

        #endregion


        #region .  Main()  .
        /// --------------------------------------------------------------------
        ///  Routine:      Main()
        ///
        ///  Description:  This is the program entry point.  The processing is:
        ///
        ///                1. Validate the command-line parameters.
        ///
        ///                2. If all are valid, continue; otherwise, show help.
        ///
        ///                3. Search for the specified file and check if the
        ///                   version is in list of vulnerable versions.  If
        ///                   so, display the file information.
        ///                   
        ///                4. Display a list of folders in which access was
        ///                denied.
        ///
        ///  Parameters:   args - string[], the command-line parameters.  These
        ///                       command-line switches are supported:
        ///
        ///                           /?
        ///                           /HELP
        ///                           /OUTPUT_FILE
        ///                           /SEARCH_FILE
        ///                           /VERSIONS
        ///
        ///  Returns:      Integer, 0 = SUCCESS; otherwise; the error code that
        ///                generated the error (see Constants.cs).
        /// --------------------------------------------------------------------
        static void Main(string[] astrArgs)
        {
            // Get the method name for use in the logging commands.
            string strMethodName = MethodBase.GetCurrentMethod().Name + "()";

            int    intReturnCode = Constants.RC_UNDEFINED;
            string strMessage    = String.Empty;
            string strStatus     = String.Empty;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                // Get the default configured filename to search for.
                m_strFileToSearchFor = Utilities.GetAppSetting(Constants.APPSETTING_KEY_SEARCH_FILE,
                                                               Constants.DEFAULT_SEARCH_FILE);

                // Get the default configured output filename.
                m_strOutputFile      = Utilities.GetAppSetting(Constants.APPSETTING_KEY_OUTPUT_FILE,
                                                               Constants.DEFAULT_OUTPUT_FILE);

                // Get the default configured flag whether to open the search results in Notepad.
                m_blnOpenResults =
                    Convert.ToBoolean(Utilities.GetAppSetting(Constants.APPSETTING_KEY_OPEN_RESULTS,
                                                              Constants.DEFAULT_OPEN_RESULTS));

                // Get the default configured flag whether to search mapped drives.
                m_blnSearchMappedDrives =
                    Convert.ToBoolean(Utilities.GetAppSetting(Constants.APPSETTING_KEY_SEARCH_MAPPED_DRIVES,
                                                              Constants.DEFAULT_SEARCH_MAPPED_DRIVES));

                // Get the default configured flag whether to show access denied paths.
                m_blnShowAccessDeniedPaths =
                    Convert.ToBoolean(Utilities.GetAppSetting(Constants.APPSETTING_KEY_SHOW_ACCESS_DENIED_PATHS,
                                                              Constants.DEFAULT_SHOW_ACCESS_DENIED_PATHS));

                // Get the default configured versions to search for.
                m_strVersions        = Utilities.GetAppSetting(Constants.APPSETTING_KEY_VERSIONS,
                                                               Constants.DEFAULT_VERSIONS);

                // Create the output file (overwrite if it exists).
                try
                {
                    m_oStreamWriter = File.CreateText(m_strOutputFile);
                }
                catch (Exception e)
                {
                    // Construct the error message.
                    strMessage = String.Format("{0}|{1}|{2}()|{3}",
                                               Constants.RC_ERROR_NUM_INVALID_PARAMETER_OUTPUT_FILE,
                                               Constants.RC_ERROR_MSG_INVALID_PARAMETER_OUTPUT_FILE +
                                                   " " + e.Message.Replace("|", ":"),
                                               MethodBase.GetCurrentMethod().Name,
                                               e.StackTrace);

                    // Throw error up the chain.
                    throw new Exception(strMessage, e.InnerException);
                }

                // If all of the command-line parameters are valid continue.
                if (IsValidCommandLineParameters(astrArgs, strMethodName))
                {
                    // Check to show help if that command-line parameter is present.
                    if (m_blnSwitch_ShowHelp == true)
                    {
                        ShowHelp();
                    }
                    else
                    {
                        // Show the version header.
                        ShowApplicationVersion(true);

                        // Display the values used for this run.
                        Console.WriteLine(String.Format("Application Start Time....:  {0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
                        Console.WriteLine(String.Format("Search For File...........:  {0}", m_strFileToSearchFor));
                        Console.WriteLine(String.Format("Versions To Check.........:  {0}", m_strVersions));
                        Console.WriteLine(String.Format("Skip Mapped Drives........:  {0}", m_blnSearchMappedDrives.ToString()));
                        Console.WriteLine(String.Format("Show Access Denied Paths..:  {0}", m_blnShowAccessDeniedPaths.ToString()));
                        Console.WriteLine(String.Format("Results Output File.......:  {0}", Path.GetFullPath(m_strOutputFile)));
                        Console.WriteLine(String.Format("Open search results.......:  {0}", m_blnOpenResults.ToString()));
                        Console.WriteLine("");
                        m_oStreamWriter.WriteLine(String.Format("Application Start Time....:  {0} {1}", DateTime.Now.ToLongDateString(), DateTime.Now.ToLongTimeString()));
                        m_oStreamWriter.WriteLine(String.Format("Search For File...........:  {0}", m_strFileToSearchFor));
                        m_oStreamWriter.WriteLine(String.Format("Versions To Check.........:  {0}", m_strVersions));
                        m_oStreamWriter.WriteLine(String.Format("Skip Mapped Drives........:  {0}", m_blnSearchMappedDrives.ToString()));
                        m_oStreamWriter.WriteLine(String.Format("Show Access Denied Paths..:  {0}", m_blnShowAccessDeniedPaths.ToString()));
                        m_oStreamWriter.WriteLine(String.Format("Results Output File.......:  {0}", Path.GetFullPath(m_strOutputFile)));
                        m_oStreamWriter.WriteLine(String.Format("Open search results.......:  {0}", m_blnOpenResults.ToString()));
                        m_oStreamWriter.WriteLine("");

                        // Start with drives if you have to search the entire computer. 
                        string[] astrDrives = System.Environment.GetLogicalDrives();

                        foreach (string strDrive in astrDrives)
                        {
                            System.IO.DriveInfo oDriveInfo = new System.IO.DriveInfo(strDrive);

                            // Check to skip the drive if it is not ready to be read.
                            if (!oDriveInfo.IsReady)
                            {
                                strMessage = String.Format("The drive {0} could not be read", oDriveInfo.Name);
                                Console.WriteLine(strMessage);
                                Console.WriteLine("");
                                m_oStreamWriter.WriteLine(strMessage);
                                m_oStreamWriter.WriteLine("");
                                continue;
                            }
                            // Check to skip a mapped drive.
                            else if ((!m_blnSearchMappedDrives) && (oDriveInfo.DriveType == DriveType.Network))
                            {
                                strMessage = String.Format("Skip searching mapped drive {0}", oDriveInfo.Name);
                                Console.WriteLine(strMessage);
                                Console.WriteLine("");
                                m_oStreamWriter.WriteLine(strMessage);
                                m_oStreamWriter.WriteLine("");
                                continue;
                            }

                            System.IO.DirectoryInfo diRootDir = oDriveInfo.RootDirectory;
                            SearchDirectoryTree(diRootDir);
                        }

                        // Check to write out all the files that could not be processed.
                        if (m_blnShowAccessDeniedPaths)
                        {
                            strMessage = "Folders with restricted access:";
                            Console.WriteLine(strMessage);
                            m_oStreamWriter.WriteLine(strMessage);

                            foreach (string strLine in m_colNoAccessLog)
                            {
                                Console.WriteLine(strLine);
                                m_oStreamWriter.WriteLine(strLine);
                            }
                            Console.WriteLine("");
                            m_oStreamWriter.WriteLine("");
                        }

                    }   // end of:  if (m_blnSwitch_ShowHelp) else ...

                    // If we get here the program ran successfully - set the return code.
                    intReturnCode = Constants.RC_SUCCESS;

                }   // end of:  if (IsValidCommandLineParameters(...))
                else
                {
                    // One or more invalid command-line parameters.
                    intReturnCode = Constants.RC_ERROR_NUM_INVALID_COMMAND_LINE_PARAMETERS;
                }
            }
            catch (Exception ex)
            {
                ShowError(ex.Message);
                ShowHelp();

                intReturnCode = Constants.RC_ERROR_NUM_UNEXPECTED_ERROR;
            }
            finally
            {
                sw.Stop();

                // Do not show the results if HELP was specified.
                if (m_blnSwitch_ShowHelp == false)
                {
                    Console.WriteLine(String.Format("Number of files searched..:  {0:N0}", m_intFileCount));
                    Console.WriteLine(String.Format("Number of vulnerable files:  {0:N0}", m_intVulnerableCount));
                    Console.WriteLine(String.Format("Application run time......:  {0}",    Utilities.ShowTime(sw, 0)));
                    Console.WriteLine("");
                    m_oStreamWriter.WriteLine(String.Format("Number of files searched..:  {0:N0}", m_intFileCount));
                    m_oStreamWriter.WriteLine(String.Format("Number of vulnerable files:  {0:N0}", m_intVulnerableCount));
                    m_oStreamWriter.WriteLine(String.Format("Application run time......:  {0}",    Utilities.ShowTime(sw, 0)));
                    m_oStreamWriter.WriteLine("");

                    Console.WriteLine(String.Format("Program {0} - Return Code:  {1}",
                                                   (intReturnCode == Constants.RC_SUCCESS) ? "ended successfully" : "failed",
                                                   intReturnCode));
                    Console.WriteLine("");

                    // Close the output file.
                    m_oStreamWriter.Close();
                }

                try
                {
                    // Get the name of the parent process that called TaskWatchdog.
                    string strParentProcess = ProcessInformation.GetParentProcessName();

                    // If running from the Visual Studio IDE or called from the
                    // Windows Explorer, I need to keep the Console window from
                    // closing after the application finished.
                    if ((strParentProcess.ToUpperInvariant() == "DEVENV.EXE") ||
                        (strParentProcess.ToUpperInvariant() == "EXPLORER.EXE"))
                    {
                        Console.Write("Running from {0} - Press [Enter] to continue...", strParentProcess);
                        Console.ReadLine();
                    }
                }
                catch { }

                // Set the return code.
                System.Environment.ExitCode = intReturnCode;

                // If not showing HELP, attempt to open the results file in Notepad.
                if ((m_blnSwitch_ShowHelp == false) &&
                    (m_blnOpenResults     == true)  &&
                    (File.Exists(m_strOutputFile)))
                {
                    Process.Start("notepad.exe", m_strOutputFile);
                }

            }   // end of:  finally

        }   // Main()
        #endregion


        /// --------------------------------------------------------------------
        /// METHODS:
        /// --------
        ///     IsFileVulnerable()
        ///     IsValidCommandLineParameters()
        ///     SearchDirectoryTree()
        ///     ShowApplicationVersion()
        ///     ShowError()
        ///     ShowHelp()
        /// --------------------------------------------------------------------

        #region .  IsFileVulnerable()  .
        /// --------------------------------------------------------------------
        ///  Routine:      IsFileVulnerable()
        ///
        ///  Description:  This routine determines if the file is vulnerable.
        ///
        ///  Parameters:   fvi - FileVersionInfo object for the file to check.
        ///
        ///  Returns:      Boolean, TRUE if the file is vulnerable; otherwise,
        ///                FALSE.
        /// --------------------------------------------------------------------
        private static bool IsFileVulnerable(FileVersionInfo oFileVersionInfo)
        {
            bool blnResult = false;

            try
            {
                // Split the versions into an array for parsing.
                string[] astrVersions = m_strVersions.ToLower().Split(',');

                // Loop through the array and see if the version of the file
                // specified is in the list of vulnerable versions.
                foreach (string strVersion in astrVersions)
                {
                    if (!String.IsNullOrEmpty(oFileVersionInfo.ProductVersion))
                    {
                        if (oFileVersionInfo.ProductVersion.ToLower() == strVersion)
                        {
                            blnResult = true;
                            break;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                // Handle all unexpected errors here.
                string strMessage =
                    String.Format("{0}|{1}|{2}()|{3}",
                                   Constants.RC_ERROR_NUM_UNEXPECTED_ERROR,
                                   Constants.RC_ERROR_MSG_UNEXPECTED_ERROR + " " + ex.Message.Replace("|", ":"),
                                   MethodBase.GetCurrentMethod().Name,
                                   ex.StackTrace);

                // Throw error up the chain.
                throw new Exception(strMessage, ex.InnerException);
            }

            // Return the result.
            return blnResult;

        }   // IsFileVulnerable()
        #endregion


        #region .  IsValidCommandLineParameters()  .
        /// --------------------------------------------------------------------
        ///  Routine:      IsValidCommandLineParameters()
        ///
        ///  Description:  This routine determines if the command-line parameters
        ///                specified are valid.  The supported parameters are:
        ///
        ///                  /?
        ///                  /HELP
        ///                  /OPEN_RESULTS
        ///                  /OUTPUT_FILE
        ///                  /SEARCH_FILE
        ///                  /VERSIONS
        ///
        ///  Parameters:   astrArgs - string[], array of command-line parameters.
        ///
        ///  Returns:      Boolean, TRUE if all required command-line parameters
        ///                are present and valid, and any optional command-line
        ///                parameters are valid; otherwise, FALSE.
        /// --------------------------------------------------------------------
        private static bool IsValidCommandLineParameters(string[] astrArgs, string strMainMethodName)
        {
            bool   blnResult = true;
            string strArg    = String.Empty;

            // Get the method name.
            string strMethodName = MethodBase.GetCurrentMethod().Name;

            try
            {
                // Zero command-line parameters is fine, the defaults will be used.
                if (astrArgs.Length != 0)
                {
                    // There are command-line parameters specified, loop through
                    // the array of command-line parameters and validate them.
                    for (int i = 0; i < astrArgs.Length; i++)
                    {
                        strArg = astrArgs[i];

                        string strKey   = null;
                        string strValue = null;

                        // Check for a key/value parameter;
                        if (strArg.Contains("="))
                        {
                            strKey   = strArg.Split('=')[0];
                            strValue = strArg.Split('=')[1];
                        }
                        else
                        {
                            strKey = strArg;
                        }

                        switch (strKey.ToUpper())
                        {
                            case "/?":
                            case "/HELP":
                                m_blnSwitch_ShowHelp = true;
                                break;

                            case "/OPEN_RESULTS":
                                if (!String.IsNullOrEmpty(strValue))
                                {
                                    switch (strValue.ToLower())
                                    {
                                        case "true":
                                        case "false":
                                            m_blnOpenResults = Convert.ToBoolean(strValue);
                                            break;
                                        default:
                                            blnResult = false;
                                            break;
                                    }
                                }
                                break;

                            case "/OUTPUT_FILE":
                                if (!String.IsNullOrEmpty(strValue))
                                {
                                    m_strOutputFile = strValue;
                                }
                                break;

                            case "/SEARCH_FILE":
                                if (!String.IsNullOrEmpty(strValue))
                                {
                                    m_strFileToSearchFor = strValue;
                                }
                                break;

                            case "/VERSIONS":
                                if (!String.IsNullOrEmpty(strValue))
                                {
                                    m_strVersions = strValue;
                                }
                                break;

                            default:
                                blnResult            = false;
                                m_blnSwitch_ShowHelp = true;
                                break;

                        }   // end of:  switch()

                        // If the HELP parameter is specified, break out of the
                        // FOR loop -- the remaining parameters (if present) are
                        // not going to be checked.
                        if (m_blnSwitch_ShowHelp)
                        {
                            break;
                        }

                    }   // end of:  for() loop

                }   // end of:  if (astrArgs.Length == 0)
            }
            catch (Exception ex)
            {
                // Handle all unexpected errors here.
                string strMessage =
                    String.Format("{0}|{1}|{2}()|{3}",
                                   Constants.RC_ERROR_NUM_UNEXPECTED_ERROR,
                                   Constants.RC_ERROR_MSG_UNEXPECTED_ERROR + " " + ex.Message.Replace("|", ":"),
                                   strMethodName,
                                   ex.StackTrace);

                // Throw error up the chain.
                throw new Exception(strMessage, ex.InnerException);
            }
            finally
            {
                if (!blnResult)
                {
                    string strMessage = String.Format("{0}|{1} [{2}]",
                                                      Constants.RC_ERROR_NUM_UNKNOWN_COMMAND_LINE_PARAMETER,
                                                      Constants.RC_ERROR_MSG_UNKNOWN_COMMAND_LINE_PARAMETER,
                                                      strArg);

                    // Throw error up the chain.
                    throw new Exception(strMessage);
                }
            }

            // Return the result.
            return blnResult;

        }   // IsValidCommandLineParameters()
        #endregion


        #region .  SearchDirectoryTree()  .
        /// --------------------------------------------------------------------
        ///  Routine:      SearchDirectoryTree()
        ///
        ///  Description:  This routine searches the directory tree for the file
        ///                and, if found, displays some details and whether it
        ///                has a version in the vulnerable versions list.
        ///
        ///  Parameters:   diRoot - DirectoryInfo, the position in the directory
        ///                         to begin searching from.
        ///
        ///  Returns:      Nothing, the console and log files are written to.
        /// --------------------------------------------------------------------
        static void SearchDirectoryTree(System.IO.DirectoryInfo diRoot)
        {
            System.IO.FileInfo[] oFiles = null;

            // First, process all the files directly under this folder .
            try
            {
                oFiles = diRoot.GetFiles("*.*");
            }
            // This is thrown if even one of the files require permissions
            // greater than the application provides. 
            catch (UnauthorizedAccessException ex)
            {
                // This code just writes out the message and continues to recurse. 
                // You may decide to do something different here. For example, you 
                // can try to elevate your privileges and access the file again.
                string strMessage = ex.Message;
                if (strMessage.StartsWith("Access to the path '"))
                {
                    strMessage = strMessage.Remove(0, 20);
                }
                int intPos = strMessage.IndexOf("' is denied.");
                if (intPos != -1)
                {
                    strMessage = strMessage.Substring(0, intPos);
                }
                m_colNoAccessLog.Add(String.Format("    {0}", strMessage));
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (oFiles != null)
            {
                foreach (System.IO.FileInfo oFile in oFiles)
                {
                    // Increment the file count.
                    m_intFileCount++;

                    // In this example, we only access the existing FileInfo object.
                    // If we want to open, delete or modify the file, then a try-catch
                    // block is required here to handle the case where the file has been
                    // deleted since the call to SearchDirectoryTree().
                    if (oFile.Name == m_strFileToSearchFor)
                    {
                        FileVersionInfo oFileVersionInfo = FileVersionInfo.GetVersionInfo(oFile.FullName);

                        // Determine if this file is vulnerable, if so increment the count.
                        bool blnVulnerable = IsFileVulnerable(oFileVersionInfo);
                        if (blnVulnerable) m_intVulnerableCount++;
                        
                        Console.WriteLine(oFile.FullName);
                        Console.WriteLine(String.Format("    File Version..........:  {0}", oFileVersionInfo.FileVersion));
                        //Console.WriteLine(String.Format("    Product Version.......:  {0}", oFileVersionInfo.ProductVersion));
                        //Console.WriteLine(String.Format("    ProductMajorPart......:  {0}", oFileVersionInfo.ProductMajorPart));
                        //Console.WriteLine(String.Format("    ProductMinorPart......:  {0}", oFileVersionInfo.ProductMinorPart));
                        //Console.WriteLine(String.Format("    ProductBuildPart......:  {0}", oFileVersionInfo.ProductBuildPart));
                        //Console.WriteLine(String.Format("    ProductPrivatePart....:  {0}", oFileVersionInfo.ProductPrivatePart));
                        Console.WriteLine(String.Format("    Vulnerable?...........:  {0}", IsFileVulnerable(oFileVersionInfo) ? "Yes" : "No"));
                        Console.WriteLine("");
                        m_oStreamWriter.WriteLine(oFile.FullName);
                        m_oStreamWriter.WriteLine(String.Format("    File Version..........:  {0}", oFileVersionInfo.FileVersion));
                        //m_oStreamWriter.WriteLine(String.Format("    Product Version.......:  {0}", oFileVersionInfo.ProductVersion));
                        //m_oStreamWriter.WriteLine(String.Format("    ProductMajorPart......:  {0}", oFileVersionInfo.ProductMajorPart));
                        //m_oStreamWriter.WriteLine(String.Format("    ProductMinorPart......:  {0}", oFileVersionInfo.ProductMinorPart));
                        //m_oStreamWriter.WriteLine(String.Format("    ProductBuildPart......:  {0}", oFileVersionInfo.ProductBuildPart));
                        //m_oStreamWriter.WriteLine(String.Format("    ProductPrivatePart....:  {0}", oFileVersionInfo.ProductPrivatePart));
                        m_oStreamWriter.WriteLine(String.Format("    Vulnerable?...........:  {0}", IsFileVulnerable(oFileVersionInfo) ? "Yes" : "No"));
                        m_oStreamWriter.WriteLine("");
                    }
                }

                // Now find all the subdirectories under this directory.
                foreach (System.IO.DirectoryInfo oDirInfo in diRoot.GetDirectories())
                {
                    // Resursive call for each subdirectory.
                    SearchDirectoryTree(oDirInfo);
                }
            }

        }   // SearchDirectoryTree()
        #endregion


        #region .  ShowApplicationVersion()  .
        /// --------------------------------------------------------------------
        ///  Routine:      ShowApplicationVersion()
        ///
        ///  Description:  This routine displays the version information for this
        ///                application.
        ///
        ///  Parameters:   None
        ///
        ///  Returns:      Nothing
        /// --------------------------------------------------------------------
        private static void ShowApplicationVersion(bool blnWriteToOutputFile)
        {
            try
            {
                string strHeader =
                    String.Format("{0} - {1}",
                                  Utilities.GetAppSetting(Constants.APPSETTING_KEY_APPLICATION_NAME,
                                                          Constants.DEFAULT_APPLICATION_NAME),
                                  Utilities.GetAppSetting(Constants.APPSETTING_KEY_APPLICATION_VERSION,
                                                          Constants.DEFAULT_APPLICATION_VERSION));

                string strLine = "".PadRight(strHeader.Length, '-');
                Console.WriteLine(strLine);
                Console.WriteLine(strHeader);
                Console.WriteLine(strLine);
                Console.WriteLine();

                if (blnWriteToOutputFile)
                {
                    m_oStreamWriter.WriteLine(strLine);
                    m_oStreamWriter.WriteLine(strHeader);
                    m_oStreamWriter.WriteLine(strLine);
                    m_oStreamWriter.WriteLine();
                }
            }
            catch (Exception ex)
            {
                int intErrorCode = Marshal.GetLastWin32Error();

                // Construct the error message.
                string strMessage = String.Format("{0}|{1}|{2}()|{3}",
                                                   intErrorCode.ToString(),
                                                   ex.Message,
                                                   MethodBase.GetCurrentMethod().Name,
                                                   ex.StackTrace);

                // Throw error up the chain.
                throw new Exception(strMessage);
            }

        }   // ShowApplicationVersion()
        #endregion


        #region .  ShowError()  .
        ///---------------------------------------------------------------------
        /// Routine:      ShowError()
        ///
        /// Description:  Displays an error message on the console.
        ///
        /// Parameters:   strErrorMessage - String, the error message to display.
        ///
        /// Returns:      Nothing
        ///---------------------------------------------------------------------
        private static void ShowError(string strErrorMessage)
        {
            try
            {
                string strMessage    = "";
                string strMessageLog = "";

                string[] astrParts = strErrorMessage.Split('|');
                if (astrParts.Length < 3)
                {
                    strMessage =
                        String.Format("\r\nAn error occured parsing the ErrorMessage string.\r\n\r\n" +
                                      "Error message string:\r\n{0}\r\n",
                                       strErrorMessage);
                }
                else
                {
                    if (astrParts.Length == 3)
                    {
                        strMessage =
                            String.Format("\r\nError {0} in routine {1}\r\n\r\n" +
                                          "Description:\r\n{2}\r\n",
                                           astrParts[0],
                                           astrParts[2],
                                           astrParts[1]);
                    }
                    else if (astrParts.Length == 4)
                    {
                        strMessage =
                            String.Format("\r\nError {0} in routine {1}\r\n\r\n" +
                                          "Description:\r\n{2}\r\n",
                                           astrParts[0],
                                           astrParts[2],
                                           astrParts[1]);
                        strMessageLog =
                            String.Format("\r\nError {0} in routine {1}\r\n\r\n" +
                                          "Description:\r\n{2}\r\n\r\n" +
                                          "Stack Trace:\r\n{3}\r\n",
                                           astrParts[0],
                                           astrParts[2],
                                           astrParts[1],
                                           astrParts[3]);
                    }
                }

                Console.WriteLine("".PadRight(79, '*'));
                Console.WriteLine(strMessage);
                Console.WriteLine("".PadRight(79, '*'));
                Console.WriteLine();

                //// If not showing HELP, display this.
                //if (m_blnSwitch_ShowHelp != false)
                //{
                    m_oStreamWriter.WriteLine("".PadRight(79, '*'));
                    m_oStreamWriter.WriteLine(strMessageLog);
                    m_oStreamWriter.WriteLine("".PadRight(79, '*'));
                    m_oStreamWriter.WriteLine();
                //}
            }
            catch (Exception ex)
            {
                int intErrorCode = Marshal.GetLastWin32Error();

                // Construct the error message.
                string strMessage = String.Format("{0}|{1}|{2}()|{3}",
                                                   intErrorCode.ToString(),
                                                   ex.Message,
                                                   MethodBase.GetCurrentMethod().Name,
                                                   ex.StackTrace);

                // Throw error up the chain.
                throw new Exception(strMessage);
            }

        }   // ShowError()
        #endregion


        #region .  ShowHelp()  .
        /// --------------------------------------------------------------------
        ///  Routine:      ShowHelp()
        ///
        ///  Description:  This routine displays the command syntax and parameter
        ///                definitions, along with several examples.  If you want
        ///                to indlude additional information to display along with
        ///                these instructions, store it in the m_strErrorMessage
        ///                global variable before calling this method.
        ///
        ///  Parameters:   None
        ///
        ///  Returns:      Nothing
        /// --------------------------------------------------------------------
        private static void ShowHelp()
        {
            try
            {
                ShowApplicationVersion(false);

                Console.WriteLine("Syntax:");
                Console.WriteLine();
                Console.WriteLine("   FileSearcher [/?] [/HELP] [/SEARCH_FILE=<filename.ext>]");
                Console.WriteLine("                [/OPEN_RESULTS=TRUE|false]");
                Console.WriteLine("                [/OUTPUT_FILE=<path to output file>]");
                Console.WriteLine("                [/VERSION=#.#.#x[,#.#.#,...]");
                Console.WriteLine();
                Console.WriteLine("where:");
                Console.WriteLine();
                Console.WriteLine("   /?, /HELP       Display this help screen.  Help is also displayed if any of");
                Console.WriteLine("                   the command-line paramaters are invalid.  This parameter is");
                Console.WriteLine("                   optional and if used all other parameters are ignored.");
                Console.WriteLine();
                Console.WriteLine("   /OPEN_RESULTS   Determine whether or not to open the search results file in");
                Console.WriteLine("                   Notepad.  The default is TRUE.");
                Console.WriteLine();
                Console.WriteLine("   /OUTPUT_FILE    The full path to the file to write the search results to.");
                Console.WriteLine("                   If the filename contains spaces you must enclose it with");
                Console.WriteLine("                   quotation marks.  The output is plain text.");
                Console.WriteLine();
                Console.WriteLine("   /SEARCH_FILE    The name of the file to search for.  This must be a valid");
                Console.WriteLine("                   filename and extension.  If the filename contains spaces");
                Console.WriteLine("                   you must enclose it with quotation marks.  No wild card");
                Console.WriteLine("                   characters are allowed.");
                Console.WriteLine();
                Console.WriteLine("   /VERSIONS       A comma-delimited list of versions to check.  For exammple:");
                Console.WriteLine("                   \"1.0.1,1.0.1a,1.0.1b\")");
                Console.WriteLine();
                Console.WriteLine("The default values for the command-line parameters are:");
                Console.WriteLine();
                Console.WriteLine("   /OPEN_RESULTS   true");
                Console.WriteLine("   /OUTPUT_FILE    SearchResults.txt");
                Console.WriteLine("   /SEARCH_FILE    ssleay32.dll");
                Console.WriteLine("   /VERSIONS       1.0.1,1.0.1a,1.0.1b,1.0.1c,1.0.1d,1.0.1e,1.0.1f");
                Console.WriteLine();
                Console.WriteLine("Examples:");
                Console.WriteLine();
                Console.WriteLine("FileSearcher");
                Console.WriteLine();
                Console.WriteLine("   Search for all occcurrences of the file \"ssleay32.dll\" that have a version");
                Console.WriteLine("   number of 1.0.1, 1.0.1a, 1.0.1b, 1.0.1c, 1.0.1d, 1.0.1e or 1.0.1f and write");
                Console.WriteLine("   the search results to the file SearchResults.txt in the current folder.");
                Console.WriteLine();
                Console.WriteLine("FileSearcher /SEARCH_FILE=ssleay32.dll");
                Console.WriteLine("             /VERSIONS=1.0.1,1.0.1a,1.0.1b");
                Console.WriteLine("             /OUTPUT_FILE=C:\\Temp\\MySearchResults.txt");
                Console.WriteLine("             /OPEN_RESULTS=false");
                Console.WriteLine();
                Console.WriteLine("   Search for all occcurrences of the file \"ssleay32.dll\" that have a version");
                Console.WriteLine("   number of 1.0.1, 1.0.1a, or 1.0.1f and write the search results to the file");
                Console.WriteLine("   C:\\Temp\\MySearchResults.txt, but do not open the file in Notepad.");
                Console.WriteLine();
                Console.WriteLine("FileSearcher /?");
                Console.WriteLine();
                Console.WriteLine("   Show the program syntax and help information.");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                int intErrorCode = Marshal.GetLastWin32Error();

                // Construct the error message.
                string strMessage = String.Format("{0}|{1}|{2}()|{3}",
                                                  intErrorCode.ToString(),
                                                  ex.Message,
                                                  MethodBase.GetCurrentMethod().Name,
                                                  ex.StackTrace);

                // Throw error up the chain.
                throw new Exception(strMessage);
            }

        }   // ShowHelp()
        #endregion


    }   // class Program

}   // namespace FileSearcher
