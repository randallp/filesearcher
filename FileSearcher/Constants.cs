﻿using System;

namespace FileSearcher
{
    public static class Constants
    {
        // ---------------------------------------------------------------------
        //  Public Enumerations:
        //  --------------------
        //    Parameter (with corresponding string array of Parameter names)
        //    ErrorCodes_Email
        // ---------------------------------------------------------------------

        #region .  Enumerations  .

        public enum Parameter
        {
            Question     = 0,
            Help         = 1,
            Console      = 2,
            Open_Results = 3,
            Search_File  = 4,
            Versions     = 5
        }
        public static string[] Parameters = new string[]
        {
            "?",
            "HELP",
            "CONSOLE",
            "OPEN_RESULTS",
            "SEARCH_FILE",
            "VERSIONS"
        };

        #endregion


        // ---------------------------------------------------------------------
        //  Public Constants:
        // ---------------------------------------------------------------------

        // Application Information Settings:
        public const string APPSETTING_KEY_APPLICATION_NAME              = "Application_Name";
        public const string APPSETTING_KEY_APPLICATION_VERSION           = "Application_Version";
        public const string APPSETTING_KEY_OPEN_RESULTS                  = "Open_Results";
        public const string APPSETTING_KEY_OUTPUT_FILE                   = "Output_File";
        public const string APPSETTING_KEY_SEARCH_FILE                   = "Search_File";
        public const string APPSETTING_KEY_SEARCH_MAPPED_DRIVES          = "Search_Mapped_Drives";
        public const string APPSETTING_KEY_SHOW_ACCESS_DENIED_PATHS      = "Show_Access_Denied_Paths";
        public const string APPSETTING_KEY_VERSIONS                      = "Versions";

        public const string DEFAULT_APPLICATION_NAME                     = "FileSearcher";
        public const string DEFAULT_APPLICATION_VERSION                  = "Version 1.0.1";
        public const string DEFAULT_OPEN_RESULTS                         = "true";
        public const string DEFAULT_OUTPUT_FILE                          = "SearchResults.txt";
        public const string DEFAULT_SEARCH_FILE                          = "ssleay32.dll";
        public const string DEFAULT_SEARCH_MAPPED_DRIVES                 = "false";
        public const string DEFAULT_SHOW_ACCESS_DENIED_PATHS             = "true";
        public const string DEFAULT_VERSIONS                             = "1.0.1,1.0.1a,1.0.1b,1.0.1c,1.0.1d,1.0.1e,1.0.1f";



        // Logging:
        public const string LOG_DEBUG_METHOD_NAME                        = "{0}::{1}()";
        public const string LOG_DEBUG_METHOD_STARTED                     = "{0} - Start";
        public const string LOG_DEBUG_METHOD_ENDED                       = "{0} - End";
        public const string LOG_DEBUG_METHOD_INFO                        = "{0} -   Info        : [{1}]";
        public const string LOG_DEBUG_METHOD_PARAM                       = "{0} -   Param {1}     : [{2}]";
        public const string LOG_DEBUG_METHOD_RETURN_VALUE                = "{0} -   Return Value: [{1}]";



        // Error codes and Messages.
        public const int    RC_SUCCESS                                   = 0;
        public const int    RC_UNDEFINED                                 = -1;

        // Configuration errors.
        public const int    RC_ERROR_NUM_INVALID_PARAMETER_OUTPUT_FILE   = 100;
        public const string RC_ERROR_MSG_INVALID_PARAMETER_OUTPUT_FILE   = "Could not create the output file.";

        public const int    RC_ERROR_NUM_INVALID_PARAMETER_SEARCH_FILE   = 101;
        public const string RC_ERROR_MSG_INVALID_PARAMETER_SEARCH_FILE   = "Invalid /SEARCH_FILE parameter - it must be a valid file name.";

        public const int    RC_ERROR_NUM_INVALID_PARAMETER_VERSION_START = 102;
        public const string RC_ERROR_MSG_INVALID_PARAMETER_VERSION_START = "Invalid /VERSIONS parameter - it must be a comma-delimited list of versions.";

        public const int    RC_ERROR_NUM_UNKNOWN_COMMAND_LINE_PARAMETER  = 103;
        public const string RC_ERROR_MSG_UNKNOWN_COMMAND_LINE_PARAMETER  = "There is an unknown command-line parameter specified.";

        public const int    RC_ERROR_NUM_INVALID_COMMAND_LINE_PARAMETERS = 104;
        public const string RC_ERROR_MSG_INVALID_COMMAND_LINE_PARAMETERS = "One or more invalid command-line parameters.";

        // Unexpected error - details will be in the Exception object.
        public const int    RC_ERROR_NUM_UNEXPECTED_ERROR                = 999;
        public const string RC_ERROR_MSG_UNEXPECTED_ERROR                = "An unexpected error occurred.";


    }   // class Constants

}   // namespace FileSearcher
