﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;

namespace FileSearcher
{
    public static class Utilities
    {
        /// --------------------------------------------------------------------
        ///  Public Methods:
        ///  ---------------
        ///    ArrayToString()
        ///    GetAppSetting()
        ///    GetMethodName()
        ///    ShowTime()
        /// --------------------------------------------------------------------

        #region .  ArrayToString()  .
        // ---------------------------------------------------------------------
        //  Routine:      ArrayToString()
        //
        //  Description:  This routine converts an array of string to a single
        //                string containing each array element, delimited with
        //                a space character.
        //
        //  Parameters:   astrArray - String[], an array of strings.
        //
        //  Returns:      String, a string of all the array elements.
        // ---------------------------------------------------------------------
        public static string ArrayToString(string[] astrArray)
        {
            // Get the method name for use in the logging commands.
            string strMethodName = MethodBase.GetCurrentMethod().Name + "()";

            StringBuilder sb = new StringBuilder();

            try
            {
                // Loop through the array and append to the results, using the
                // space character as the delimiter.
                foreach (string strItem in astrArray)
                {
                    sb.Append(strItem);
                    sb.Append(" ");
                }
            }
            catch (Exception ex)
            {
                // Handle all unexpected errors here.
                string strMessage =
                    String.Format("{0}|{1}|{2}|{3}",
                                   Constants.RC_ERROR_NUM_UNEXPECTED_ERROR,
                                   Constants.RC_ERROR_MSG_UNEXPECTED_ERROR +
                                        " " +
                                        ex.Message.TrimEnd(Environment.NewLine.ToCharArray()).Replace("|", ":"),
                                   strMethodName,
                                   ex.StackTrace);

                // Throw new exception.
                throw new Exception(strMessage, ex.InnerException);
            }

            return sb.ToString().Trim();

        }   // ArrayToString()
        #endregion


        #region .  GetAppSetting()  .
        // --------------------------------------------------------------------
        //  Routine:      GetAppSetting()
        //
        //  Description:  This routine returns the value of application setting
        //                (in the App.config file) specified in the strSetting
        //                parameter, or the value specified in the strDefault
        //                parameter if the setting is not found.
        //
        //  Parameters:   strSetting - string, name of the setting key to get.
        //
        //                strDefault - string, value to return if the setting
        //                             key is not found.
        //
        //  Returns:      string, the value of the AppSetting, or the default
        //                value if the setting is not found.
        // --------------------------------------------------------------------
        public static string GetAppSetting(string strSetting, string strDefault)
        {
            // Get the method name for use in the logging commands.
            string strMethodName = GetMethodName(MethodBase.GetCurrentMethod());

            string strResult = null;

            try
            {
                // Check to assign the default value if the setting is not found in the config file.
                if (ConfigurationManager.AppSettings[strSetting] != null)
                {
                    strResult = ConfigurationManager.AppSettings[strSetting].ToString();
                }
                else
                {
                    strResult = strDefault;
                }
            }
            catch (Exception ex)
            {
                int intErrorCode = Marshal.GetLastWin32Error();

                // Construct the error message.
                string strMessage =
                    String.Format("{0}|{1}|{2}{3}",
                                   intErrorCode.ToString(),
                                   ex.Message.TrimEnd(Environment.NewLine.ToCharArray()),
                                   strMethodName,
                                   ex.StackTrace);

                // Throw custom error up the chain.
                throw new Exception(strMessage);
            }

            return strResult;

        }   // GetAppSetting()
        #endregion


        #region .  GetMethodName()  .
        // ---------------------------------------------------------------------
        //  Routine:      GetMethodName()
        //
        //  Description:  This routine returns the namespace and method name of
        //                the currently running method.
        //
        //  Parameters:   oMethodBase - the currently running method.
        //
        //  Returns:      string, the namespace and method name currently running.
        // ---------------------------------------------------------------------
        public static string GetMethodName(MethodBase oMethodBase)
        {
            return String.Format(Constants.LOG_DEBUG_METHOD_NAME,
                                 oMethodBase.DeclaringType.FullName,
                                 oMethodBase.Name);

        }   // GetMethodName()
        #endregion


        #region .  ShowTime()  .
        // ---------------------------------------------------------------------
        //  Routine:      ShowTime()
        //
        //  Description:  This routine returns a friendly string containing the
        //                elapsed time on the stopwatch.  The elapsed time is
        //                formatted as follows:
        //
        //                  {0} hours {1} minutes {2} seconds {3} milliseconds
        //
        //  Parameters:   sw - a StopWatch object.
        //
        //  Returns:      string, the friendly time elapsed on the stopwatch.
        // ---------------------------------------------------------------------
        public static string ShowTime(Stopwatch sw, int intPad)
        {
            // Get the method name for use in the logging commands.
            string strMethodName = GetMethodName(MethodBase.GetCurrentMethod());

            string strTimeString = String.Empty;

            try
            {
                TimeSpan ts = sw.Elapsed;
                strTimeString =
                    String.Format("{0}{1} hour{2} {3} minute{4} {5} second{6} {7} millisecond{8}",
                                  "".PadLeft(intPad),
                                  ts.Hours,
                                 (ts.Hours == 1) ? "" : "s",
                                  ts.Minutes,
                                 (ts.Minutes == 1) ? "" : "s",
                                  ts.Seconds,
                                 (ts.Seconds == 1) ? "" : "s",
                                  ts.Milliseconds,
                                 (ts.Milliseconds == 1) ? "" : "s");
            }
            catch (Exception ex)
            {
                // Handle all unexpected errors here.
                string strMessage =
                    String.Format("{0}|{1} {2}|{3}|{4}",
                                   Constants.RC_ERROR_NUM_UNEXPECTED_ERROR,
                                   Constants.RC_ERROR_MSG_UNEXPECTED_ERROR,
                                   ex.Message.TrimEnd(Environment.NewLine.ToCharArray()).Replace("|", ":"),
                                   strMethodName,
                                   ex.StackTrace);

                // Pass my custom error up the chain.
                throw new Exception(strMessage, ex.InnerException);
            }

            // Return the result.
            return strTimeString;

        }   // ShowTime()
        #endregion


    }   // class Utilities

}   // namespace FileSearcher
